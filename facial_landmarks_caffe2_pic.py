
# import the necessary packages
from imutils import face_utils
import numpy as np
import argparse
import imutils
import dlib
import cv2
import face_recognition
import sys
import os
import time

from xml.dom import minidom
import xml.etree.ElementTree as ET

sys.path.append(os.getcwd())
import model
import mlp_embeddings


# construct the argument parser and parse the arguments
ap = argparse.ArgumentParser()
ap.add_argument("-p", "--shape-predictor", required=True,
	help="path to facial landmark predictor")
ap.add_argument("-f", "--folder", required=True,
	help="path to folder")


args = vars(ap.parse_args())

# initialize dlib's face detector (HOG-based) and then create
# the facial landmark predictor
print ('\n********************************************')
print ('loading face detector')
detector = dlib.get_frontal_face_detector()
predictor = dlib.shape_predictor(args["shape_predictor"])


# load the input image, resize it, and convert it to grayscale
print ('\n********************************************')
print ('loading test model')

init_pb = './net/init_net.pb'
predict_pb = './net/predict_net.pb'

mnist_model = mlp_embeddings.Caffe2MNIST('face_cnn_deploy')
mnist_model.LoadPreTrainedModels(init_pb, predict_pb)


print ('\n********************************************')
print ('loading labels')
mydoc = minidom.parse('./net/OutputLabels.xml')
items = mydoc.getElementsByTagName('item')
 
labels = []

if len(items) > 0:
	for i in range(len(items)):  
		lbl = items[i].childNodes[0].data
		labels.append(str(lbl))


dataset_list = []

folder = args["folder"]

for filename in os.listdir(folder):
	dataset_list.append(folder + '/' + filename)


for img_src in dataset_list: 
	image = cv2.imread(img_src)

	if image is not None:
		print(img_src)

		image = imutils.resize(image, width=320)
		gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

		rects = detector(gray, 1)
		

		if rects is not None:

			# loop over the face detections
			for (i, rect) in enumerate(rects):
				shape = predictor(gray, rect)
				shape = face_utils.shape_to_np(shape)

				(x, y, w, h) = face_utils.rect_to_bb(rect)
				boxes = [(y, x+w, y+h, x)]				

				encoding = face_recognition.face_encodings(image, boxes)
				num,acc = mnist_model.Run(encoding[0],"data")

				x = boxes[0][3]
				y = boxes[0][0]
		
				acc = acc*100

				# show the face label
				if acc > 90:
					lbl2show = labels[num] + ': ' + str(round(acc,2))		
				else:
					lbl2show = 'Unk.' + labels[num] + ': ' + str(round(acc,2))

				cv2.rectangle(image, (x, y), (x + w, y + h), (0, 255, 0), 2)
				cv2.rectangle(image, (boxes[0][3],boxes[0][0]), (boxes[0][1], boxes[0][2]), (0, 0, 255), 2)

				cv2.putText(image, lbl2show, (x - 10, y - 10), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 255, 0), 2)

			cv2.imshow(img_src, image)


cv2.waitKey(0)
cv2.destroyAllWindows()

