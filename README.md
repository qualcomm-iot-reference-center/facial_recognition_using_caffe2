# Facial recognition on DragonBoard 410c, using Caffe2

Project developed in the IoT Reference Center, a partnership between Qualcomm and Facens. Facial recognition using caffe2, embedded in the DragonBoard 410c development board.

# Introduction

The project was developed to demonstrate the possibilities of application of the most recent machine learning libraries, such as Caffe2, in the embedded system of the DragonBoard 410c.

## Caffe2

The Caffe2 framework seeks to provide a light and direct way for the developer to experiment with and deploy Deep Learning solutions to their projects and to the local community. The Facebook-created opensource framework includes large cloud-size solutions and portability for embedded systems.
Caffe2 has APIs for Python and C ++, enabling agility in prototyping and ease of optimization. To learn more, visit the tutorials and model repository, visit the [site](https://caffe2.ai/).

## Facial recognition

The facial recognition process is a subject of study in the field of computer vision and has been gaining prominence recently. The idea of ​​having the computer interpret images on its own is a complex process, the subject of many academic and commercial applications.

Our project aims to recognize people registered through the video captured by a webcam. The operation consists of collecting a frame of the video at each desired interval of time for processing. The image is converted to grayscale and submitted to the face detector, which is a [Dlib](http://dlib.net/) library method.
If faces are detected, a second process is triggered on the CPU, so the first one is not interrupted. In the new process, each face found is treated independently in parallel processes, so all results is determined at the same instant. The treatment consists in using the image of the detached face of the original photo as the entrance of an MLP constructed with Caffe2. The network classifies the image with a confidence level, and based on this level the program determines whether the person was recognized as someone registered in the database, or if it is unknown. The result is displayed on the screen as a colored square around the face and the corresponding name determined by the inference process.
After the cycle, the process repeats indefinitely at each time interval.


# Project intallation

## Python 2.7 dependencies

Install the required packages on the DragonBoard 410c:

```sh
sudo python -m pip install imutils
sudo apt-get install python-opencv
```

In addition, it is necessary to install Caffe2 according to the procedure indicated [in this tutorial](https://gitlab.com/qualcomm-iot-reference-center/caffe2-installation-and-test).


Finally, install the package_recognitgion. To do this, you must have a SWAP region of at least 4 GB. Follow this [tutorial](https://gitlab.com/qualcomm-iot-reference-center/utilidades/blob/master/configura%C3%A7%C3%A3o_SDcard(en).md) fo the instructions.

```sh
sudo apt-get install libjpeg-dev libtiff5-dev libpng-dev -y
sudo apt-get install libavcodec-dev libavformat-dev libswscale-dev libv4l-dev -y
sudo apt-get install libxvidcore-dev libx264-dev -y
sudo apt-get install libgtk2.0-dev libgtk-3-dev -y
sudo apt-get install libatlas-base-dev gfortran -y
sudo apt-get install hdf5* libhdf5*

sudo python -m pip install face_recognition
```

It should be noted that this procedure takes at least 3 hours to complete.


## Execution

Clone the project repository for the board:

```sh
git clone https://gitlab.com/qualcomm-iot-reference-center/facial_recognition_using_caffe2
cd facial_recognition_using_caffe2
```

> To train a custom network, check the following tutorial: [MLP for Facial Recognition using Caffe2](https://gitlab.com/qualcomm-iot-reference-center/mlp-for-facial-recognition-using-caffe2). Copy the file generated to the project folder on the board.



The application can be executed in three ways:

1. With images stored in a directory;
2. Getting images from a camera;
3. Using a server to update the recognition base according to demand.

## History and evolution of the project

* First version

The first version uses the local database to sort the identified people into images recorded in a directory. To do this, just run the application indicating the location of the directory. The result of the operation is indicated by the marking of a rectangle around the recognized face, presenting the person's name and the degree of confidence of the classifier. 

```sh
bash pics.sh "<path>"
```

* Second version

The second version uses the same sturture shown in the previous item. However, images are taken directly from a camera. The application monitors the camera by taking some photos according to a timing parameter.
In this case, the recognition operates according to the parameterized rate. For example, the command below will start the application with the /dev/video2 capture device and acquisition time of 1 second.

```sh
bash webcam.sh 2 1
```

* Third version

This release features a client / server architecture to automate the facial recognition system upgrade procedure. The server operates to add new people to the recognition base and shares the precompiled templates in a shared directory.
Using an ftp client, the application running on DragonBoard410c monitors the shared directory every 15 seconds. If any change was made the network is restarted.


```sh
bash server.sh 2 "user" "password" "server IP"
```