# Reconhecimento Facial na DragonBoard 410c, utilizando Caffe2

Projeto desenvolvido no IoT Reference Center, parceria entre Qualcomm e Facens. Reconhecimento facial utilizando caffe2, embarcado na placa de desenvolvimento DragonBoard 410c.

# Teoria

O projeto foi desenvolvido para demonstrar as possibilidades de aplicação das bibliotecas de machine learning mais recentes, como Caffe2, no sistema embarcado da DragonBoard 410c.

## Caffe2

O framework Caffe2 busca prover uma maneira leve e direta para o desenvolvedor experimentar e implementar soluções de Deep Learning, fomentando a comunidade da área. O framework opensource criado pelo Facebook contempla soluções de grande porte para servidores na nuvem, e portabilidade para sistemas embarcados. 
Caffe2 possui APIs para Python e C++, possibilitando a agilidade na prototipagem e a facilidade na otimização. Para saber mais, acessar os tutoriais e o repositório de modelos, visite o [site](https://caffe2.ai/).

## Reconhecimento facial

O processo de reconhecimento facial é tema de estudo na área de visão computacional e vem ganhando destaque recentemente. A ideia de fazer o computador interpretar imagens por conta própria é um processo complexo, tema de diversas aplicações acadêmicas e comerciais.

Nosso projeto tem como objetivo reconher pessoas cadastradas através do video captado por uma webcam. O funcionamento consiste em coletar um frame do video a cada intervalo de tempo desejado para processamento. A imagem é convertida em escala de cinza e submetida ao detector facial, que é um método da biblioteca [Dlib](http://dlib.net/).
Caso sejam detectadas faces, um segundo processo é disparado na CPU, para que o primeiro não seja interrompido. No novo processo, cada face encontrada é tratada independentemente em processos paralelos, para que o resultado de todas seja determinado no mesmo instante. O tratamento consiste em utilizar a imagem da face destacada da foto original como entrada de uma MLP construída com Caffe2. A rede classifica a imagem com um nível de confiança, e baseado neste nível o programa determina se a pessoa foi reconhecida como alguém registrado no banco de dados, ou se é um desconhecido. O resultado é exibido  na tela como um quadrado colorido ao redor da face e o nome correspondente determinado pelo processo de inferência.
Após o ciclo, o processo se repete indefinidamente a cada intervalo de tempo.


# Instalação do projeto

## Dependências de pacote para python 2.7

Instale os pacotes necessários na DragonBoard 410c:

```sh
sudo python -m pip install imutils
sudo apt-get install python-opencv
```

Além disso, é necessário instalar o Caffe2 conforme o procedimento indicado [neste tutorial](https://gitlab.com/qualcomm-iot-reference-center/caffe2-installation-and-test).


Por fim, instale o pacote_recognitgion. Para tal, é necessário ter uma região de SWAP de pelo menos 4 GB.

```sh
sudo apt-get install libjpeg-dev libtiff5-dev libpng-dev -y
sudo apt-get install libavcodec-dev libavformat-dev libswscale-dev libv4l-dev -y
sudo apt-get install libxvidcore-dev libx264-dev -y
sudo apt-get install libgtk2.0-dev libgtk-3-dev -y
sudo apt-get install libatlas-base-dev gfortran -y
sudo apt-get install hdf5* libhdf5*

sudo python -m pip install face_recognition
```

Cabe ressaltar que esse procedimento demora ao menos 3 horas para ser finalizado.


## Execução

Clone o repositório do projeto para a placa:
```sh
git clone https://gitlab.com/qualcomm-iot-reference-center/facial_recognition_using_caffe2
cd facial_recognition_using_caffe2
```

> Para treinar uma rede personalizada, verifique o seguinte tutorial: [MLP for Facial Recognition using Caffe2](https://gitlab.com/qualcomm-iot-reference-center/mlp-for-facial-recognition-using-caffe2). Copie o arquivo gerado para o diretório do projeto na placa.



Essa aplicação pode ser executada de três maneiras:

1. Com imagens armazenadas em um diretório;
2. Obtendo imagens a partir de uma câmera;
3. Utilizando um servidor para atualizar a base de reconhecimento de acordo com a demanda.

## Histórico e evolução do projeto

* Primeira versão

A primeira versão utiliza a base de reconehcimento local para classificar as pessoas identificadas em imagens gravadas em um diretório. Para tal, basta executar a aplicação indicando a localização do diretório. O resultado da operação é indicado com a marcação de um retângulo em torno da face reconhecida, apresentando o nome da pessoa e o grau de confiança do classificador. 

```sh
bash pics.sh "<path>"
```

* Segunda versão

A segunda versão utiliza a mesma esturtura apresentada no item anterior. No entanto, as imagens são obtidas diretamente de uma câmera. A aplicação monitora a câmera obtendo algumas fotos conforme um parâmetro de temporização.
Nesse caso, o reconhecimento opera conforme a taxa parametrizada. Por exemplo, o comando abaixo iniciará a aplicação com o dispositivo de captura /dev/video2 e tempo de aquisição de 1 segundo.

```sh
bash webcam.sh 2 1
```

* Terceira versão

Essa versão apresenta uma arquitetura cliente/servidor para automatizar o procedimento de atualização do sistema de reconhecimento facial. O servidor opera para adicionar novas pessoas na base de reconhecimento e compartilha os modelos pré-compilados em um diretório compartilhado.
Usando um cliente ftp, a aplicação executada na DragonBoard410c monitora o diretório compartilhado a cada 15 segundos. Se alguma alteração foi realizada a rede é inicializada novamente.


```sh
bash server.sh 2 "user" "password" "server IP"
```