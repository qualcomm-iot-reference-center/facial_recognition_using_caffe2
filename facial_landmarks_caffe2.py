# import the necessary packages
import imutils
from imutils import face_utils
import dlib
import face_recognition
import pickle
import numpy as np
import cv2
import random
import argparse
import sys,os

from xml.dom import minidom
import xml.etree.ElementTree as ET
import time
from multiprocessing import Pool, Process, Manager,Event,Queue

sys.path.append(os.getcwd())
import model
import mlp_embeddings


def VideoCapture(cam, interval, q, event):

	print ('\n********************************************')
	print ('starting video capture')
	print ('********************************************')

	#starts video capture
	cv2.namedWindow("Camera")
	cv2.resizeWindow("Camera", 200,200)
	vobj = cv2.VideoCapture(int(cam))

	start = time.time()
	init = 0

	while True: 
		#reads a new frame
		status, image = vobj.read()

		if (image is not None):
			#updates timer interval
			end = time.time()

			#evaluates the time interval
			if ((end - start) >= interval):

				#Takes a snapshot only if the internal flag is true
				#or if this is the first iteration

				if (init == 0) or (event.is_set()):
					
					#resizes the image and convert it to gray color
					img = imutils.resize(image, width=600, height=600)

					gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

					#try to detect faces int the image
					rects = detector(gray, 1)

					if (rects is not None):
						#restarts timer interval
						start = time.time()

						#passes the image for the consumer process
						q.put([img.copy(), gray, rects])
						
						#Resets the internal flag to false
						if(init == 1):
							event.clear()

					#sets the first iteration flag
					init = 1
				
			#shows the current image caught by camera
			image = imutils.resize(image, width=200, height=200)
			cv2.imshow("Camera", image)

		#gets keypressed'event
		key = (cv2.waitKey(1) & 0xFF)

		if (key == ord('q')):
			#signals consumer to exit process
			q.put([None, None, None])
			vobj.release()
			break
 

def FacialRecognizer(image,gray,rect,procnum,my_return):
	shape = predictor(gray, rect)
	shape = face_utils.shape_to_np(shape)

	# convert dlib's rectangle to a OpenCV-style
	(x, y, w, h) = face_utils.rect_to_bb(rect)
	boxes = [(y, x+w, y+h, x)]				
	
	encoding = face_recognition.face_encodings(image, boxes)

	#gets the output from Caffe2
	num,acc = mlp.Run(encoding[0],"data")
	acc = acc*100

	my_return[procnum] = [boxes[0],num,acc]


def FeatureExtraction(q,event):
	cv2.namedWindow("Snapshot",cv2.WINDOW_NORMAL)
	cv2.resizeWindow("Snapshot", 600,600)

	colours = []
	for i in range(20):
	 colours.append ([int(random.random()*255),int(random.random()*255),int(random.random()*255)])


	while True:
		#waits for a new image from producer process
		image, gray, rects = q.get()

		if image is None:
			#breaks the process
			break


		parallel_process = []
		manager = Manager()
		return_dict = manager.dict()

		#starts a new process for each face detected
		for (i, rect) in enumerate(rects):
			parallel_process.append(Process(target=FacialRecognizer, args=(image,gray,rect,i,return_dict)))
			parallel_process[i].start()


		last_area = 0

		#waits for processes completion
		for (i, p) in enumerate(parallel_process):
			p.join()

			#gets the result of current process
			boxes = return_dict[i][0]
			num = return_dict[i][1]
			acc = return_dict[i][2]

			x = boxes[3]
			y = boxes[0]

			# show the face label
			if acc > 90:
				lbl2show = labels[num] + ': ' + str(round(acc,2))				
			else:
				lbl2show = 'Unk.' + labels[num] + ': ' + str(round(acc,2))

 
			cv2.rectangle(image, (x,y), (boxes[1], boxes[2]), colours[i], 2)
			cv2.putText(image, lbl2show, (10,image.shape[0]-5 -i*20), cv2.FONT_HERSHEY_SIMPLEX, 1, colours[i], 5)

			area = boxes[1]*boxes[2]
			if (area > last_area):
				last_area = area

		#notifies the producer process that this task have been completed
		event.set()

		# shows the output image
		#image = imutils.resize(image, width=600, height=600)
		cv2.imshow("Snapshot", image)

		key = (cv2.waitKey(1) & 0xFF)


if __name__ == '__main__':

	# construct the argument parser and parse the arguments
	ap = argparse.ArgumentParser()
	ap.add_argument("-p", "--shape-predictor", required=True,
		help="path to facial landmark predictor")
	ap.add_argument("-i", "--image", required=True,
		help="path to input image")
	ap.add_argument("-t", "--frameInterval", required=True,
		help="image Processing Interval in milliseconds")
	args = vars(ap.parse_args())

	# Default imageProcessing interval in seconds
	imageProcessingInterval = int(args["frameInterval"])


	# initialize dlib's face detector (HOG-based) and then create
	# the facial landmark predictor
	print ('\n********************************************')
	print ('loading face detector')
	print ('********************************************')
	detector = dlib.get_frontal_face_detector()
	predictor = dlib.shape_predictor(args["shape_predictor"])


	print ('\n********************************************')
	print ('loading caffe model')
	print ('********************************************')

	init_pb = './net/init_net.pb'
	predict_pb = './net/predict_net.pb'

	mlp = mlp_embeddings.Caffe2MNIST('face_cnn_deploy')
	mlp.LoadPreTrainedModels(init_pb, predict_pb)


	print ('\n********************************************')
	print ('loading labels')
	print ('********************************************')
	mydoc = minidom.parse('./net/OutputLabels.xml')
	items = mydoc.getElementsByTagName('item')
	 
	labels = []

	if len(items) > 0:
		for i in range(len(items)):  
			lbl = items[i].childNodes[0].data
			print(lbl)
			labels.append(str(lbl))

	#shared signal between prod/consumer 
	evt = Event()

	#shared data between prod/consumer 
	q = Queue()

	process_one = Process(target=VideoCapture, args=(args["image"], imageProcessingInterval, q, evt))
	process_two = Process(target=FeatureExtraction, args=(q,evt))

	#starts prod/consumer process
	process_one.start()
	process_two.start()

	#release shared data
	q.close()
	q.join_thread()

	#waits for the end of process
	process_one.join()
	process_two.join()

	
	cv2.destroyAllWindows()
