import caffe2.python.predictor.predictor_exporter as pe
import caffe2.python.predictor.predictor_py_utils as pu
import caffe2.proto.predictor_consts_pb2 as predictor_consts
from caffe2.proto import caffe2_pb2
from caffe2.python import core, workspace

class Caffe2Model:

	def __init__(self):
		core.GlobalInit(['caffe2', '--caffe2_log_level=0'])
		self.device_opts = core.DeviceOption(caffe2_pb2.CPU, 0)
		
	def LoadPreTrainedModels(self,init_net_pb,predict_net_pb):
		
		workspace.ResetWorkspace()


		init_def = caffe2_pb2.NetDef()

		with open(init_net_pb, 'r') as f:
			init_def.ParseFromString(f.read())
			init_def.device_option.CopyFrom(self.device_opts)
			workspace.RunNetOnce(init_def.SerializeToString())
			
		net_def = caffe2_pb2.NetDef()
		with open(predict_net_pb, 'r') as f:
			net_def.ParseFromString(f.read())
			net_def.device_option.CopyFrom(self.device_opts)
			workspace.CreateNet(net_def.SerializeToString(), overwrite=True)


	def Run(self, data, blob):
		pass


